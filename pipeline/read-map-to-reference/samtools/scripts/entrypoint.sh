# go add readgroup using picard before continuing

# check that SAM file is valid; nothing logged means it's valid
samtools quickcheck -v /results/sequence-alignment-maps/SRR13741501/result_with_rg.sam

#  Convert SAM to BAM
samtools view -bS /results/sequence-alignment-maps/SRR13741501/result_with_rg.sam > /results/sequence-alignment-maps/SRR13741501/result_with_rg.bam

# sort the BAM file
samtools sort /results/sequence-alignment-maps/SRR13741501/result_with_rg.bam -o /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.bam

# use picard to remove duplicates
# see stats
samtools flagstat /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam

# EXAMPLE REPORT
# 68843837 + 0 in total (QC-passed reads + QC-failed reads)
# 66588720 + 0 primary
# 0 + 0 secondary
# 2255117 + 0 supplementary
# 3765590 + 0 duplicates
# 3765590 + 0 primary duplicates
# 59456717 + 0 mapped (86.36% : N/A) # 86.36% of reads mapped; you can de novo assemble the rest which may not have had an homologous sequence in the reference genome
# 57201600 + 0 primary mapped (85.90% : N/A)
# 66588720 + 0 paired in sequencing
# 33294360 + 0 read1
# 33294360 + 0 read2
# 51908230 + 0 properly paired (77.95% : N/A)
# 55632662 + 0 with itself and mate mapped
# 1568938 + 0 singletons (2.36% : N/A)
# 3481022 + 0 with mate mapped to a different chr
# 1210335 + 0 with mate mapped to a different chr (mapQ>=5)

#### STARTING HERE, CODE IS EXPERIMENTAL AND NOT REALLY DOING WHAT I WANT

# consensus caller
samtools consensus -a -f fasta /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam > /results/sequence-alignment-maps/SRR13741501/consensus_contigs.fasta

####

# use bedtools to extract contigs from mapped reads
bedtools genomecov -ibam /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam -bga | awk '$4 > 0' > /results/sequence-alignment-maps/SRR13741501/covered_regions.bed
bedtools intersect -abam /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam -b /results/sequence-alignment-maps/SRR13741501/covered_regions.bed > /results/sequence-alignment-maps/SRR13741501/mapped_reads.bam

# convert BAM to FASTA
samtools fasta /results/sequence-alignment-maps/SRR13741501/mapped_reads.bam > /results/sequence-alignment-maps/SRR13741501/mapped_reads.fasta

# Assemble contigs from these mapped reads
cap3 mapped_reads.fasta -o 40 -p 90

##### 
samtools mpileup -aa -f /genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021.fasta /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam > /results/sequence-alignment-maps/SRR13741501/pileup.txt


awk '
BEGIN {OFS="\n"; contig_num=1}
$4 > 0 {
    if (seq == "") {
        printf ">contig_%d\n", contig_num
    }
    seq = seq $5
    next
}
seq != "" {
    print seq
    seq = ""
    contig_num++
}
END {
    if (seq != "") print seq
}
' /results/sequence-alignment-maps/SRR13741501/pileup.txt > /results/sequence-alignment-maps/SRR13741501/split_contigs.fasta

sed -i 's/[^ATCGatcg]//g; s/[atcg]/\U&/g' /results/sequence-alignment-maps/SRR13741501/split_contigs.fasta

###   
# Call variants by identifying differences between the sample and the reference genome
bcftools mpileup -Ou -f /genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021.fasta /results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.deduped.bam | bcftools call -mv -Ob -o /results/sequence-alignment-maps/SRR13741501/called_variants.bcf
# filtering variants
bcftools filter -i 'QUAL>20 && DP>5' -Ob -o /results/sequence-alignment-maps/SRR13741501/filtered_variants.bcf /results/sequence-alignment-maps/SRR13741501/called_variants.bcf
# consensus
bcftools index /results/sequence-alignment-maps/SRR13741501/filtered_variants.bcf
bcftools consensus -f /genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021.fasta -o /results/sequence-alignment-maps/SRR13741501/bcf_consensus.fasta /results/sequence-alignment-maps/SRR13741501/filtered_variants.bcf