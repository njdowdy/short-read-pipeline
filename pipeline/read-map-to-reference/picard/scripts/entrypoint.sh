
java -Xmx8G -jar /usr/picard/picard.jar CreateSequenceDictionary \
  R=/genomes/Spilarctia_lutea_GCA_916048165.1_ilSpiLutu1.1_ChromosomeLevel_2021.fasta \
  O=/genomes/Spilarctia_lutea_GCA_916048165.1_ilSpiLutu1.1_ChromosomeLevel_2021.dict

java -Xmx8G -jar /usr/picard/picard.jar ValidateSamFile \
  I=/results/sequence-alignment-maps/ENT5135a/result.sam \
  O=/results/sequence-alignment-maps/ENT5135a/result.sam.validation_report.txt \
  R=/genomes/Spilarctia_lutea_GCA_916048165.1_ilSpiLutu1.1_ChromosomeLevel_2021.fasta \
  MODE=SUMMARY

java -jar /usr/picard/picard.jar ValidateSamFile \
  I=/results/sequence-alignment-maps/SRR13741501/result.sam \
  O=/results/sequence-alignment-maps/SRR13741501/result.sam.validation_report.txt \
  R=/genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021.fasta \
  MODE=SUMMARY

# if the ValidateSamFile command reports an error, try removing reference genome; it's a less complete report, but it will run at least

java -jar /usr/picard/picard.jar ValidateSamFile \
  I=/results/sequence-alignment-maps/SRR13741501/result.sam \
  O=/results/sequence-alignment-maps/SRR13741501/result.sam.validation_report.txt \
  MODE=SUMMARY

# if the ValidateSamFile command reports missing read groups, try:
java -jar /usr/picard/picard.jar AddOrReplaceReadGroups \
  I=/results/sequence-alignment-maps/SRR13741501/result.sam \
  O=/results/sequence-alignment-maps/SRR13741501/result_with_rg.sam \
  RGID=1 \
  RGLB=lib1 \
  RGPL=ILLUMINA \
  RGPU=unit1 \
  RGSM=SRR13741501

# Identify and mark duplicate reads, which can arise from PCR amplification
java -jar /usr/picard/picard.jar MarkDuplicates I=/results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.bam O=/results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.markdup.bam M=/results/sequence-alignment-maps/SRR13741501/marked_dup_metrics.txt

# remove duplicates
# ? removing duplicates can affect coverage depth. It's generally considered a best practice for variant calling
java -jar /usr/picard/picard.jar MarkDuplicates \
      I=/results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.bam \
      O=/results/sequence-alignment-maps/SRR13741501/result_with_rg.sorted.deduped.bam \
      M=/results/sequence-alignment-maps/SRR13741501/marked_dup_metrics.txt \
      REMOVE_DUPLICATES=true