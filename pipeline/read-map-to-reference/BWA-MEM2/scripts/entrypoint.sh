# Indexing the reference sequence (Requires 7N GB memory where N is the size of the reference sequence).
# /bwa-mem2-2.2.1_x64-linux/bwa-mem2 index [-p prefix] <in.fasta>
# Where 
# <in.fasta> is the path to reference sequence fasta file and 
# <prefix> is the prefix of the names of the files that store the resultant index. Default is in.fasta.
#! Test with ENT5315 Spilosoma lutea
mkdir /results/bwa-mem2-indexed-genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021/
/bwa-mem2-2.2.1_x64-linux/bwa-mem2 index -p /results/bwa-mem2-indexed-genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021/index /genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021.fasta

# Mapping 
# Run "/bwa-mem2-2.2.1_x64-linux/bwa-mem2 mem" to get all options
# /bwa-mem2-2.2.1_x64-linux/bwa-mem2 mem -t <num_threads> <prefix> <reads.fq/fa> > out.sam
# Where <prefix> is the prefix specified when creating the index or the path to the reference fasta file in case no prefix was provided.
#! Test with ENT5315 Spilosoma lutea
mkdir /results/sequence-alignment-maps/SRR13741501/
/bwa-mem2-2.2.1_x64-linux/bwa-mem2 mem -t 6 /results/bwa-mem2-indexed-genomes/Hypena_proboscidalis_GCA_905147285.1_ilHypProb1.1_ChromosomeLevel_2021/index /reads/Hypena_baltimoralis/SRR13741501_qualityTrimmed_1.fastq.gz /reads/Hypena_baltimoralis/SRR13741501_qualityTrimmed_2.fastq.gz > /results/sequence-alignment-maps/SRR13741501/result.sam

