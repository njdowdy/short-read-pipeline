makeblastdb -in "/data/contigs.fasta" -dbtype nucl -out "/results/db-wgs24_0003"

echo "BLAST database created successfully."

blastn -query "/targets/ARCTA235-07_Agylla_argentifera.fasta" -db "/results/db-wgs24_0003" -task megablast -out "/results/blastn_results.out"