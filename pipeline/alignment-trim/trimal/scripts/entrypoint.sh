echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎞️  Alignment trimming started! This will take a few minutes..." | tee -a /logs/log.txt

# run python script
python3 /bioinformatics/bioinformatics/trimal_workflow.py

# replace "X" with "?" in text in sequences in all files in /results/trimmed/bmge/*
mkdir -p /results/trimmed/bmge-fixed
for file in /results/trimmed/bmge/*; do
    # get output file name
    output=$(echo $file | sed 's/bmge/bmge-fixed/g')
    awk '/^ +[a-zA-Z]+/ {gsub("X","?",$0)} {print}' $file > $output
done

echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎉 Alignment Trimming Completed! 🎉" | tee -a /logs/log.txt