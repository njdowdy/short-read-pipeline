# Merges the contigs found in Abyss, SPAdes and megahit assemblies into a single file suitable for further
# processing with Phyluce phylogeny tools.

# Takes the UCES from various assembly runs and creates a seperate file taking only the best sequence per contig

# For each specimen, add all the contigs to a single dictionary from every file, then examine each contig sequence and
# choose the one with the greatest length.