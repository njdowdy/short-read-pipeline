# get the filenames of every file in /data and store in /scripts/in1.txt without file extension (.fasta)
ls /data | sed 's/\.fasta$//' > /scripts/in1.txt

# get the filenames of every file in /results and store in /scripts/out1.txt without file extension (.aln)
ls /results | sed 's/\.aln$//' | sed 's/$/\.unaligned/' > /scripts/out1.txt

# find the text that is in /scripts/in1.txt that are not in /scripts/out1.txt
comm -23 /scripts/in1.txt /scripts/out1.txt > /scripts/diff.txt

# for each file in diff run muscle3 with that file as input and output as /results/<filename>.aln
while read line; do
    # generate filename
    line=$line.fasta
    # get the length of the shortest sequence
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    # get the length of the shortest sequence
    shortest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (min=="" || $1 < min) {min=$1}END{print min}')
    # get the number of lines in $line that start with ">"
    num_sequences=$(grep -c "^>" /data/$line)
    # replace ".unaligned." in $filename with ""
    output=$(echo "$line" | sed 's/.unaligned.fasta//g').aln
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 💪 starting MUSCLEv3 alignment of: $line (ntaxa: $num_sequences; lengths: $shortest-$longest bp)...\n🤖  MUSCLEv3 Output:\n" | tee -a /logs/log.txt
    # run muscle
    muscle -in /data/$line -out /results/$output
    # new length
    longest=$(tr -d '\n' < /results/$output | grep -o "[A|C|T|G|N|-]\+" | wc -L)
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏁 MUSCLEv3 finished alignment of: $line (new length: $longest bp)!\n" | tee -a /logs/log.txt
done < /scripts/diff.txt

# write success message to log file
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎉 MUSCLEv3 Completed! 🎉" | tee -a /logs/log.txt

# delete intermediate files
rm /scripts/in1.txt /scripts/out1.txt /scripts/diff.txt