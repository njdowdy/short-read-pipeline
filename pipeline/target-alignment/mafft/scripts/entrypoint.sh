# make output dirs in case they were not created
mkdir -p /results/linsi
mkdir -p /results/default
mkdir -p /results/einsi
mkdir -p /results/ginsi
# get the filenames of every file in /data and store in /scripts/in1.txt without file extension (.fasta)
ls /data | sed 's/\.fasta$//' > /scripts/in1.txt

# get the filenames of every file in /results/* and store in /scripts/out-*.txt without file extension (.aln)
ls /results/linsi | sed 's/\.aln$//' | sed 's/$/\.unaligned/' > /scripts/out-default.txt
ls /results/linsi | sed 's/\.aln$//' | sed 's/$/\.unaligned/' > /scripts/out-linsi.txt
ls /results/einsi | sed 's/\.aln$//' | sed 's/$/\.unaligned/' > /scripts/out-einsi.txt
ls /results/ginsi | sed 's/\.aln$//' | sed 's/$/\.unaligned/' > /scripts/out-ginsi.txt

# find the text that is in /scripts/in1.txt that are not in /scripts/out-*.txt
comm -23 /scripts/in1.txt /scripts/out-linsi.txt > /scripts/diff-default.txt
comm -23 /scripts/in1.txt /scripts/out-linsi.txt > /scripts/diff-linsi.txt
comm -23 /scripts/in1.txt /scripts/out-einsi.txt > /scripts/diff-einsi.txt
comm -23 /scripts/in1.txt /scripts/out-ginsi.txt > /scripts/diff-ginsi.txt

# set thread count
threads=$(nproc)

# for each file in diff-default run mafft with that file as input and output as /results/default/<filename>.aln
while read line; do
    # generate filename
    line=$line.fasta
    # get the length of the shortest sequence
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    # get the length of the shortest sequence
    shortest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (min=="" || $1 < min) {min=$1}END{print min}')
    # get the number of lines in $line that start with ">"
    num_sequences=$(grep -c "^>" /data/$line)
    # replace ".unaligned." in $filename with ""
    output=$(echo "$line" | sed 's/.unaligned.fasta//g').aln
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 starting MAFFTv7 shared 6-mer pair alignment of: $line (ntaxa: $num_sequences; lengths: $shortest-$longest bp)...\n🤖  MAFFTv7 Output:\n" | tee -a /logs/log.txt
    # run mafft
    mafft --preservecase --thread $threads --maxiterate 1000 /data/$line > /results/default/$output | tee -a /logs/log.txt
    # new length
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /results/default/$output | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏁 MAFFTv7 finished shared 6-mer pair alignment of: $line (new length: $longest bp)!\n" | tee -a /logs/log.txt
done < /scripts/diff-default.txt

# for each file in diff-linsi run mafft with that file as input and output as /results/linsi/<filename>.aln
while read line; do
    # generate filename
    line=$line.fasta
    # get the length of the shortest sequence
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    # get the length of the shortest sequence
    shortest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (min=="" || $1 < min) {min=$1}END{print min}')
    # get the number of lines in $line that start with ">"
    num_sequences=$(grep -c "^>" /data/$line)
    # replace ".unaligned." in $filename with ""
    output=$(echo "$line" | sed 's/.unaligned.fasta//g').aln
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 starting MAFFTv7 local pair alignment (Smith-Waterman) with affine gap costs (Gotoh) (L-INS-i) alignment of: $line (ntaxa: $num_sequences; lengths: $shortest-$longest bp)...\n🤖  MAFFTv7 Output:\n" | tee -a /logs/log.txt
    # run mafft
    mafft --preservecase --thread $threads --maxiterate 1000 --localpair /data/$line > /results/linsi/$output | tee -a /logs/log.txt
    # new length
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /results/linsi/$output | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏁 MAFFTv7 finished local pair alignment (Smith-Waterman) with affine gap costs (Gotoh) (L-INS-i) alignment of: $line (new length: $longest bp)!\n" | tee -a /logs/log.txt
done < /scripts/diff-linsi.txt

# for each file in diff-einsi run mafft with that file as input and output as /results/einsi/<filename>.aln
while read line; do
    # generate filename
    line=$line.fasta
    # get the length of the shortest sequence
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    # get the length of the shortest sequence
    shortest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (min=="" || $1 < min) {min=$1}END{print min}')
    # get the number of lines in $line that start with ">"
    num_sequences=$(grep -c "^>" /data/$line)
    # replace ".unaligned." in $filename with ""
    output=$(echo "$line" | sed 's/.unaligned.fasta//g').aln
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 starting MAFFTv7 local pair alignment with generalized affine gap costs (Altschul) (E-INS-i) alignment of: $line (ntaxa: $num_sequences; lengths: $shortest-$longest bp)...\n🤖  MAFFTv7 Output:\n" | tee -a /logs/log.txt
    # run mafft
    mafft --preservecase --thread $threads --maxiterate 1000 --genafpair /data/$line > /results/einsi/$output | tee -a /logs/log.txt
    # new length
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /results/einsi/$output | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏁 MAFFTv7 finished local pair alignment with generalized affine gap costs (Altschul) (E-INS-i) alignment of: $line (new length: $longest bp)!\n" | tee -a /logs/log.txt
done < /scripts/diff-einsi.txt

# for each file in diff-ginsi run mafft with that file as input and output as /results/ginsi/<filename>.aln
while read line; do
    # generate filename
    line=$line.fasta
    # get the length of the shortest sequence
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    # get the length of the shortest sequence
    shortest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /data/$line | awk '$1 != ">" && (min=="" || $1 < min) {min=$1}END{print min}')
    # get the number of lines in $line that start with ">"
    num_sequences=$(grep -c "^>" /data/$line)
    # replace ".unaligned." in $filename with ""
    output=$(echo "$line" | sed 's/.unaligned.fasta//g').aln
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 starting MAFFTv7 global alignment (Needleman-Wunsch) (G-INS-i) alignment of: $line (ntaxa: $num_sequences; lengths: $shortest-$longest bp)...\n🤖  MAFFTv7 Output:\n" | tee -a /logs/log.txt
    # run mafft
    mafft --preservecase --thread $threads --maxiterate 1000 --genafpair /data/$line > /results/ginsi/$output | tee -a /logs/log.txt
    # new length
    longest=$(awk '/^>/ {if (seqlen){print seqlen}; seqlen=0;next; } { seqlen += length($0)}END{if (seqlen){print seqlen};}' /results/ginsi/$output | awk '$1 != ">" && (max=="" || $1 > max) {max=$1}END{print max}')
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏁 MAFFTv7 finished global alignment (Needleman-Wunsch) (G-INS-i) alignment of: $line (new length: $longest bp)!\n" | tee -a /logs/log.txt
done < /scripts/diff-ginsi.txt

# write success message to log file
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎉 MAFFTv7 Completed! 🎉" | tee -a /logs/log.txt

# delete intermediate files
rm /scripts/in1.txt /scripts/out*.txt /scripts/diff*.txt