import sys
from pymsa import (
    MSA,
    Entropy,
    PercentageOfNonGaps,
    PercentageOfTotallyConservedColumns,
    Star,
    SumOfPairs,
)
from pymsa import PAM250, Blosum62, FileMatrix
from pymsa.util.fasta import print_alignment


def run_all_scores(sequences: list, print_res: bool = False) -> list:
    # uppercase all nucleotides in the sequences
    sequences = [(pair[0], pair[1].upper()) for pair in sequences]

    aligned_sequences = list(pair[1] for pair in sequences)
    sequences_id = list(pair[0] for pair in sequences)

    msa = MSA(aligned_sequences, sequences_id)

    # Percentage of non-gaps and totally conserved columns
    non_gaps = PercentageOfNonGaps(msa)
    totally_conserved_columns = PercentageOfTotallyConservedColumns(msa)

    percentage = non_gaps.compute()
    conserved = totally_conserved_columns.compute()
    entropy = Entropy(msa).compute()
    sups_blosum62 = SumOfPairs(msa, Blosum62()).compute()
    sups_pam250 = SumOfPairs(msa, PAM250()).compute()
    sups_pam380 = SumOfPairs(msa, FileMatrix("PAM380.txt")).compute()
    star_blosum62 = Star(msa, Blosum62()).compute()
    star_pam250 = Star(msa, PAM250()).compute()
    star_pam380 = Star(msa, FileMatrix("PAM380.txt")).compute()

    if print_res:
        # print alignment
        print_alignment(msa)
        # print all the scores computed
        print("Percentage of non-gaps: {0} %".format(percentage))
        print("Percentage of totally conserved columns: {0}".format(conserved))
        print("Entropy score: {0}".format(entropy))
        print("Sum of Pairs score (Blosum62): {0}".format(sups_blosum62))
        print("Sum of Pairs score (PAM250): {0}".format(sups_pam250))
        print("Sum of Pairs score (PAM380): {0}".format(sups_pam380))
        print("Star score (Blosum62): {0}".format(star_blosum62))
        print("Star score (PAM250): {0}".format(star_pam250))
        print("Star score (PAM380): {0}".format(star_pam380))

    # return a list of all the scores computed
    return {
        "non-gap percentage": percentage,
        "totally conserved columns": conserved,
        "entropy": 1 / entropy,
        "sups_blosum62": sups_blosum62,
        "sups_pam250": sups_pam250,
        "sups_pam380": sups_pam380,
        "star_blosum62": star_blosum62,
        "star_pam250": star_pam250,
        "star_pam380": star_pam380,
    }


def compare_scores(alignment_name: str, print_res: bool = False) -> str:
    # for each of /muscle3/results/<alignment_name>, /muscle5/results/<alignment_name>, and /mafft/results/default/<alignment_name> parse sequences from the fasta formatted .aln file provided in the sys.argv and pass to run_all_scores()
    scores = {}
    for file in [
        f"/muscle5/results/{alignment_name}",
        f"/muscle3/results/{alignment_name}",
        f"/mafft/results/default/{alignment_name}",
        f"/mafft/results/linsi/{alignment_name}",
        f"/mafft/results/einsi/{alignment_name}",
        f"/mafft/results/ginsi/{alignment_name}",
    ]:
        # create a job key by splitting the file path on /, removing any entries of "results", and joining the remaining parts with _
        job_key = file
        sequences = []
        with open(file, "r") as f:
            for line in f:
                if line.startswith(">"):
                    sequences.append((line.strip()[1:], ""))
                else:
                    sequences[-1] = (sequences[-1][0], sequences[-1][1] + line.strip())
        res = run_all_scores(sequences, print_res)
        # append the scores to the scores dictionary with the job_key as the key
        scores[job_key] = res
    # print(scores)
    # find the job key that has the largest number of highest values in all the scores
    max_score = 0
    max_score_key = ""
    first = True
    for key in scores:
        score = 0
        for score_key in scores[key]:
            # optionally weight the scores by the score_key name (e.g., entropy may be more important than non-gap percentage)
            # here, I'm focusing on entropy, totally conserved columns, and star_blosum62
            if key == "non-gap percentage":
                weight = 0
            elif key == "totally conserved columns":
                weight = 1
            elif key == "entropy":
                weight = 1
            elif key == "sups_blosum62":
                weight = 0
            elif key == "sups_pam250":
                weight = 0
            elif key == "sups_pam380":
                weight = 0
            elif key == "star_blosum62":
                weight = 1
            elif key == "star_pam250":
                weight = 0
            elif key == "star_pam380":
                weight = 0
            else:
                weight = 1
            score += scores[key][score_key] * weight
        if first:
            max_score = score
            max_score_key = key
            first = False
        else:
            if score > max_score:
                max_score = score
                max_score_key = key
        # print(f"{key}: {score} ... {max_score_key}: {max_score}")
    # print the job key that has the largest number of highest values in all the scores
    # print(f"Best alignment: {max_score_key}")
    # return the job key that has the largest number of highest values in all the scores
    return max_score_key


if __name__ == "__main__":
    # take in the sequence filename from the command line invocation of the script like: python runner.py 1e94.aln --verbose
    if len(sys.argv) == 3:
        if sys.argv[2] == "--verbose":
            print_res = True
        else:
            print_res = False
    elif len(sys.argv) == 2:
        print_res = False
    else:
        print("Usage: python runner.py <sequence_filename> print_res=<True|False>")
        sys.exit(1)
    res = compare_scores(sys.argv[1], print_res)
    # pass res on to the shell script calling this script
    print(res)
    sys.exit(0)
