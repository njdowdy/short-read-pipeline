# make alignment output directory
mkdir -p /results

# get the filenames of every file in in /muscle5/results and store in /scripts/in1.txt
ls /muscle5/results > /scripts/in1.txt

# get the filenames of every file in /results and store in /scripts/out1.txt
ls /results > /scripts/out1.txt

# find the text that is in /scripts/in1.txt that are not in /scripts/out1.txt and store in /scripts/diff.txt
diff /scripts/in1.txt /scripts/out1.txt | grep "<" | awk '{print $2}' > /scripts/diff.txt

# for each file in /scripts/diff.txt run pyMSA
for file in $(cat /scripts/diff.txt)
do
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 starting DNA alignment comparison of: $file..." | tee -a /logs/log.txt
    # store the result of /scripts/runner.py $file in a variable
    result=$(python /scripts/runner.py $file)
    # copy the result to /results and overwrite any existing files
    cp -f $result /results
    # from the result, get whether it contains muscle3, muscle5, or mafft, and if mafft determine whether it is mafft-linsi or mafft-ginsi, etc
    if echo "$result" | grep -q "muscle5"; then
        version="muscle5"
    elif echo "$result" | grep -q "muscle3"; then
        version="muscle3"
    elif echo "$result" | grep -q "mafft.*default"; then
        version="mafft-default"
    elif echo "$result" | grep -q "mafft.*linsi"; then
        version="mafft-linsi"
    elif echo "$result" | grep -q "mafft.*einsi"; then
        version="mafft-einsi"
    elif echo "$result" | grep -q "mafft.*ginsi"; then
        version="mafft-ginsi"
    fi
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🧬 completed DNA alignment comparison of: $file...$version was chosen!" | tee -a /logs/log.txt
done

# write success message to log file
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎉 Alignment QC Completed! 🎉" | tee -a /logs/log.txt

# delete intermediate files
rm /scripts/in1.txt /scripts/out1.txt /scripts/diff.txt