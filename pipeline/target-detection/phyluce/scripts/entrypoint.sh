# MANUAL ENVIRONMENT SET UP
# set -e
# conda activate phyluce
# exec "$@"

# 1 - MATCH PROBES AGAINST ASSEMBLED CONTIGS

phyluce_assembly_match_contigs_to_probes \
    --contigs /spades/trimmed/data \
    --probes  /targets/zfmk2_probes.fasta \
    --output /phyluce_output/ | tee -a /logs/log.txt
# copy results out
cp /phyluce_output/* /results/

# 2 - GENERATE A LIST OF UCE LOCI WE ENRICHED IN EACH TAXON
#  create the data matrix configuration file
# !: DOWNSTREAM RESULTS DEPEND ON THE TAXON-GROUP SELECTED HERE
# !: FEWER UCEs MAY BE RECOVERED WITH BROAD IN-GROUPS
# !: TRY TO SELECT A TAXON-GROUP THAT IS AS SPECIFIC AS POSSIBLE FOR YOUR RESEARCH QUESTION
mkdir -p /phyluce_output/taxon-sets/all
phyluce_assembly_get_match_counts \
    --locus-db /phyluce_output/probe.matches.sqlite \
    --taxon-list-config /targets/arctiinae-taxon-set.conf \
    --taxon-group 'arctiinae' \
    --incomplete-matrix \
    --output /phyluce_output/taxon-sets/all/all-taxa-incomplete.conf | tee -a /logs/log.txt
# copy results out
cp /phyluce_output/taxon-sets/all/all-taxa-incomplete.conf /results/all-taxa-incomplete.conf

# 3 - EXTRACT FASTA DATA CORRESPONDING TO THE LOCI IN all-taxa-incomplete.conf

# get FASTA data for taxa in our taxon set
phyluce_assembly_get_fastas_from_match_counts \
    --contigs /spades/trimmed/data \
    --locus-db /phyluce_output/probe.matches.sqlite \
    --match-count-output /phyluce_output/taxon-sets/all/all-taxa-incomplete.conf \
    --output /phyluce_output/taxon-sets/all/all-taxa-incomplete.fasta \
    --incomplete-matrix /phyluce_output/taxon-sets/all/all-taxa-incomplete.incomplete \
    --log-path /logs | tee -a /logs/log.txt
# copy results out
cp /phyluce_output/taxon-sets/all/all-taxa-incomplete.fasta /results/all-taxa-incomplete.fasta
cp /phyluce_output/taxon-sets/all/all-taxa-incomplete.incomplete /results/all-taxa-incomplete.incomplete

# 4 - EXPLODE THE MONOLITHIC FASTA BY TAXON
phyluce_assembly_explode_get_fastas_file \
    --input /phyluce_output/taxon-sets/all/all-taxa-incomplete.fasta \
    --output /phyluce_output/exploded-fastas/taxon \
    --by-taxon | tee -a /logs/log.txt
# copy results out
mkdir -p /results/byTaxon/
cp /phyluce_output/exploded-fastas/taxon/* /results/byTaxon/
# get summary stats on the FASTAs
echo "Obtaining summary statistics by taxon..." | tee -a /logs/log.txt
echo "samples,contigs,total bp,mean length,95 CI length,min length,max length,median length,contigs >1kb" | tee -a /logs/results_byTaxon.csv
for i in /phyluce_output/exploded-fastas/taxon/*.fasta;
do
    phyluce_assembly_get_fasta_lengths --input $i --csv | tee -a /logs/log.txt | tee -a /logs/results_byTaxon.csv
done

# 5 - EXPLODE THE MONOLITHIC FASTA BY LOCUS
phyluce_assembly_explode_get_fastas_file \
    --input /phyluce_output/taxon-sets/all/all-taxa-incomplete.fasta \
    --output /phyluce_output/exploded-fastas/locus | tee -a /logs/log.txt
# copy results out
mkdir -p /results/byLocus/
cp /phyluce_output/exploded-fastas/locus/* /results/byLocus/
# get summary stats on the FASTAs
echo "Obtaining summary statistics by locus..." | tee -a /logs/log.txt
echo "locus,taxa,total bp,mean length,95 CI length,min length,max length,median length,taxa >1kb" | tee -a /logs/results_byLocus.csv
for i in /phyluce_output/exploded-fastas/locus/*.fasta;
do
    phyluce_assembly_get_fasta_lengths --input $i --csv | tee -a /logs/log.txt | tee -a /logs/results_byLocus.csv
done

# 6 - OPTIONAL - FILTER OUT ALIGNMENTS WITH FEWER THAN N TAXA
# Phyluce recommends 75% and 95% completeness
# I have seen 50% commonly in the literature
# Personally I think low completeness is ok, but YMMV
total_taxa=$(ls /results/byTaxon/ | wc -l)
thirty_percent_taxa=$(awk "BEGIN { printf \"%.0f\", $total_taxa * 0.3 + 0.5 }")
# fifty_percent_taxa=$(awk "BEGIN { printf \"%.0f\", $total_taxa * 0.5 + 0.5 }")
# but you have to have at least 2 taxa in the alignment
if [ $thirty_percent_taxa -gt 1 ]; then
    threshold=$thirty_percent_taxa
else
    threshold=2
fi
mkdir -p /results/byLocus-30pct
for file in /phyluce_output/exploded-fastas/locus/*; do
    if [[ $(grep -c "^>" "$file") -ge $threshold ]]; then
        cp $file /results/byLocus-30pct/
    fi
done