import re
from typing import Optional, List
from Bio import SeqIO
from Bio.SeqIO import SeqRecord
from Bio import AlignIO
from Bio.Align import AlignInfo


def generate_consensus(fasta: str, threshold: Optional[float] = 0.5):
    """Generate simple consensus sequence from a fasta file
    Output will be sent to '<parent_dir>/<sequence_set_name>/<locus_name>_{threshold}%_consensus.fasta'
    Args:
        fasta (str): location of fasta file to generate consensus from of the form: '<parent_dir>/<sequence_set_name>/<locus_name>.fasta'
        threshold (Optional[float], optional): If the percentage of the most common residue type is greater then the passed threshold, then that residue type will be used as the consensus, otherwise "N" will be added. Defaults to 0.5.
    """
    # fasta = '<parent_dir>/<sequence_set_name>/<locus_name>.fasta'
    # fasta = "bioinformatics/input/fasta/noc1-all/L1.fasta"
    # fasta = 'bioinformatics/output/taxon_filtered_alignments/ZFMK1/EOG090R0A51_3.final.out.fas'
    directory = "/".join(fasta.split("/")[0:-1])  # '<parent_dir>/<sequence_set_name>'
    locus_name = fasta.split("/")[-1].split(".")[0]
    sequence_set_name = fasta.split("/")[-2]
    output_file = f"{directory}/{locus_name}_{round(threshold*100)}%_consensus.fasta"
    try:
        alignment = AlignIO.read(fasta, "fasta")
    except:
        print("Alignment was empty or malformed. Skipping.")
        pass
    else:
        summary_align = AlignInfo.SummaryInfo(alignment)
        consensus = summary_align.dumb_consensus(threshold, "N")
        my_seqs = SeqRecord(
            consensus,
            id="",
            description=f"{sequence_set_name}_{locus_name}_{threshold*100}%_consensus",
        )
        SeqIO.write(my_seqs, output_file, "fasta")
