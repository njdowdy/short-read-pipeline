import os

# from Bio import SeqIO

# from Bio import AlignIO
# from bioinformatics.functions.file_utils import copy_directory

# from bioinformatics.functions.tasks import (
#     generate_consensus_seqs,
#     create_blast_dbs,
#     replace_char_in_filenames,
#     apply_single_fasta_blast,
#     apply_fasta_clean_newlines,
# )
from bioinformatics.functions.blast import parse_blast_result

# # assumes it looks like this: "bioinformatics/input/fasta/NOC1_PHASE2/"
# db_seqs_input_folder = "bioinformatics/input/fasta/noc1-all/"
# query_seqs_input_folder = "/spades/trimmed/data"

# # convert noc1-all from phylip to fasta and fix the filenames (only run if needed)
# # for file in os.listdir(db_seqs_input_folder):
# #     if file.endswith(".phylip"):
# #         new_name = file.split("T657_Dowdy_Erebidae5_")[1].split(".")[0]  # locus name
# #         for alignment in AlignIO.parse(
# #             f"{db_seqs_input_folder}T657_Dowdy_Erebidae5_{new_name}.phylip",
# #             "phylip-relaxed",
# #         ):
# #             SeqIO.write(alignment, f"{db_seqs_input_folder}{new_name}.fasta", "fasta")


# # set of consensus sequences to combine
# # because we are querying taxon by taxon, we do not need a consensus for queries
# db_seqs_consensus_location = "bioinformatics/output/consensus_sequences/noc1-all/"

# # where to write the consensus set into a single fasta
# db_seqs_concat_consensus = "bioinformatics/input/reference_seqs/consensus_seqs.fasta"

# # set of consensus sequences to blast
# db_seqs = "bioinformatics/input/reference_seqs/"

# # generate consensus sequences for each locus
# generate_consensus_seqs(db_seqs_input_folder)

# # merge the consensus sequences into one file
# with open(db_seqs_concat_consensus, "w") as w_file:
#     for file in os.listdir(db_seqs_consensus_location):
#         with open(f"{db_seqs_consensus_location}{file}", "rU") as o_file:
#             seq_records = SeqIO.parse(o_file, "fasta")
#             SeqIO.write(seq_records, w_file, "fasta")

# # generate BLAST database for each reference (folder containing 1 fasta for each taxon/kit to be blasted)
# create_blast_dbs(db_seqs)

# # step 8: BLAST each locus > parse top hit as extracted region
# extraction_folder = "bioinformatics/output/seq_extractions/noc1-all/"  # folder containing non-consensus seqs of query taxa?
# copy_directory(db_seqs_input_folder, extraction_folder)  # not sure what this is doing
# ref_blast_db = "bioinformatics/output/blastdb/consensus_seqs/"
# apply_single_fasta_blast(
#     input_folder=query_seqs_input_folder,
#     ref_blast_db=ref_blast_db,
#     extraction_folder=extraction_folder,
#     dcmegablast=False,
# )
# # apply_fasta_clean_newlines(extraction_folder)

# file location
folder = "bioinformatics/output/blast_hits/blastn/data_VS_consensus_seqs/"
# parse results
for file in os.listdir(folder):
    parse_blast_result(f"{folder}{file}")
