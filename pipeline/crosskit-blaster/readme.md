# !: This is not optimized yet, but should be fine for first pass

- For each gene in NOC1 probe kit, create a consensus sequence

- For each gene in target probe kit, create a consensus sequence
- reciprocal BLAST the two against each other and find the best hits
  - anything that passes 1:1 in both directions is gets assigned to each other target_locus605 -> NOC1_locus001
- based on the mapping, tack on the target_locus### data to the NOC1_locus### data
- realign and trim alignments
- go make a phylogeny
