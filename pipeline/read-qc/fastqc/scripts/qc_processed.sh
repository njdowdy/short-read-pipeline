mkdir /scripts/processed/

# get list of files in /processed_data
find /processed_data -name "*.fastq.gz" > /scripts/processed/in1.txt

# get list of files in /results/processed
find /results/processed -name "*_fastqc.html" > /scripts/processed/out1.txt

# extract "*" from "/processed_data/*.fastq.gz"
cat /scripts/processed/in1.txt | sed 's/\/processed_data\///g' | sed 's/\.fastq\.gz//g' > /scripts/processed/in2.txt

# extract "*" from "/results/processed/*_fastqc.html"
cat /scripts/processed/out1.txt | sed 's/\/results\/processed\///g' | sed 's/_fastqc\.html//g' > /scripts/processed/out2.txt

# sort the text in /scripts/processed/in2.txt and /scripts/processed/out2.txt alphabetically
sort /scripts/processed/in2.txt > /scripts/processed/in3.txt
sort /scripts/processed/out2.txt > /scripts/processed/out3.txt

# get lines that are present in /scripts/processed/in3.txt but not in /scripts/processed/out3.txt
comm -23 /scripts/processed/in3.txt /scripts/processed/out3.txt > /scripts/processed/toProcess_.txt

# prepend /processed_data/ and append .fastq.gz to /scripts/processed/toProcess.txt
cat /scripts/processed/toProcess_.txt | sed 's/^/\/processed_data\//g' | sed 's/$/\.fastq\.gz/g' > /scripts/processed/toProcess.txt

# delete intermediate "in and "out" files
rm /scripts/processed/in*.txt
rm /scripts/processed/out*.txt
rm /scripts/processed/toProcess_.txt

# only run the following if there are files to process based on text in the "toProcess" file
if [ -s /scripts/processed/toProcess.txt ]
then
    # for each file in the "toProcess" file, display the message: "Starting <file>!"
    while read -r filename
    do
        # get file size of $line in MB
        sizeMB=$(du -h $filename | cut -f1)
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        echo "$timestamp :: 🛫 launching analysis of: $filename ($sizeMB)..." | tee -a /logs/log.txt
        # begin a timer to track how long it takes to run fastqc on the file
        SECONDS=$(date +%s)
        # call fastqc on the files in /scripts/processed/toProcess.txt
        fastqc -q $filename -o /results/processed
        # check elapsed time since timer was started
        duration=$(($(date +%s)-$SECONDS))
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        echo "$timestamp :: 🛬 completed analysis of: $filename in $duration seconds!" | tee -a /logs/log.txt
    done < /scripts/processed/toProcess.txt
else
    # get current timestamp
    timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
    echo "$timestamp :: 🎉 FastQC Processing Completed! 🎉" | tee -a /logs/log.txt
    exit 0
fi

# delete intermediate "toProcess" file
rm /scripts/processed/toProcess.txt

# Send a message to the user
# get current timestamp
timestamp=$(date '+%Y-%m-%d_%H-%M-%S(%Z)')
echo "$timestamp :: 🏁 FastQC finished on processed reads! Check the /results/processed folder." | tee -a /logs/log.txt
