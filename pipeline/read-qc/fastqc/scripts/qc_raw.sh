# get list of files in /data
find /data -name "*.fastq.gz" > /scripts/raw/in1.txt

# get list of files in /results/raw
find /results/raw -name "*_fastqc.html" > /scripts/raw/out1.txt

# extract "*" from "/data/*.fastq.gz"
cat /scripts/raw/in1.txt | sed 's/\/data\///g' | sed 's/\.fastq\.gz//g' > /scripts/raw/in2.txt

# extract "*" from "/results/raw/*_fastqc.html"
cat /scripts/raw/out1.txt | sed 's/\/results\/raw\///g' | sed 's/_fastqc\.html//g' > /scripts/raw/out2.txt

# sort the text in /scripts/raw/in2.txt and /scripts/raw/out2.txt alphabetically
sort /scripts/raw/in2.txt > /scripts/raw/in3.txt
sort /scripts/raw/out2.txt > /scripts/raw/out3.txt

# get lines that are present in /scripts/raw/in3.txt but not in /scripts/raw/out3.txt
comm -23 /scripts/raw/in3.txt /scripts/raw/out3.txt > /scripts/raw/toProcess_.txt

# prepend /data/ and append .fastq.gz to /scripts/raw/toProcess.txt
cat /scripts/raw/toProcess_.txt | sed 's/^/\/data\//g' | sed 's/$/\.fastq\.gz/g' > /scripts/raw/toProcess.txt

# delete intermediate "in and "out" files
rm /scripts/raw/in*.txt
rm /scripts/raw/out*.txt
rm /scripts/raw/toProcess_.txt

# only run the following if there are files to process based on text in the "toProcess" file
if [ -s /scripts/raw/toProcess.txt ]
then
    # for each file in the "toProcess" file, display the message: "Starting <file>!"
    while read -r filename
    do
        # get file size of $line in MB
        sizeMB=$(du -h $filename | cut -f1)
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        echo "$timestamp :: 🛫 launching analysis of: $filename ($sizeMB)..." | tee -a /logs/log.txt
        # begin a timer to track how long it takes to run fastqc on the file
        SECONDS=$(date +%s)
        # call fastqc on the files in /scripts/raw/toProcess.txt
        fastqc -q $filename -o /results/raw
        # check elapsed time since timer was started
        duration=$(($(date +%s)-$SECONDS))
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        echo "$timestamp :: 🛬 completed analysis of: $filename in $duration seconds!" | tee -a /logs/log.txt
    done < /scripts/raw/toProcess.txt
else
    # get current timestamp
    timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
    echo "$timestamp :: 🎉 FastQC Processing Completed! 🎉" | tee -a /logs/log.txt
    exit 0
fi

# delete intermediate "toProcess" file
rm /scripts/raw/toProcess.txt

# Send a message to the user
# get current timestamp
timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
echo "$timestamp :: 🏁 FastQC finished on raw reads! Check the /results/raw folder." | tee -a /logs/log.txt
