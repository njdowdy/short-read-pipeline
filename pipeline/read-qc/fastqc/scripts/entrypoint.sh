chmod +x /scripts/qc_raw.sh
chmod +x /scripts/qc_processed.sh

# process existing files in the /raw_data directory
sh /scripts/qc_raw.sh

# process existing files in the /processed_data directory
sh /scripts/qc_processed.sh