#!/bin/bash

# Define variables for customizable parameters
DATA_FOLDER="/data"
REPORT_NAME="fastp_report"
FILE_1_SUFFIX="_1.fastq.gz"
FILE_2_SUFFIX="_2.fastq.gz"
TRIMMED_FILENAME_LABEL="_fastpTrimmed"
WORKERS=10

# Function to get current timestamp
timestamp() {
  date +"%Y-%m-%d %H:%M:%S"
}

# Function to process a single subfolder
process_subfolder() {
    full_path="$1"
    
    # Extract subfolder name without the full path
    subfolder=$(basename "$full_path")
    
    # Check if fastp_report.html already exists
    if [[ -f "${full_path}/${REPORT_NAME}.html" ]]; then
        echo "Skipping $subfolder: ${REPORT_NAME}.html already exists"
        return 0
    fi
    
    # Find files ending with FILE_1_SUFFIX and FILE_2_SUFFIX
    file1=$(find "$full_path" -name "*${FILE_1_SUFFIX}" | head -n 1)
    file2=$(find "$full_path" -name "*${FILE_2_SUFFIX}" | head -n 1)
    
    # Check if both files exist
    if [[ -n $file1 && -n $file2 ]]; then
        # Extract the base name without FILE_1_SUFFIX
        base=$(basename "$file1" "$FILE_1_SUFFIX")
        
        # Construct output file names
        out1="${full_path}/${base}${TRIMMED_FILENAME_LABEL}${FILE_1_SUFFIX}"
        out2="${full_path}/${base}${TRIMMED_FILENAME_LABEL}${FILE_2_SUFFIX}"
        merged="${full_path}/${base}${TRIMMED_FILENAME_LABEL}_merged.fastq.gz"
        unmerged="${full_path}/${base}${TRIMMED_FILENAME_LABEL}_unmerged.fastq.gz"
        html="${full_path}/${REPORT_NAME}.html"
        json="${full_path}/${REPORT_NAME}.json"
        log="${full_path}/fastp.log"
        
        # Log start of analysis
        start_time=$(timestamp)
        echo "[$start_time] Starting analysis of ${subfolder}/${base}${FILE_1_SUFFIX} and ${subfolder}/${base}${FILE_2_SUFFIX}" | tee "$log"
        
        # Run fastp command and time it
        start_seconds=$SECONDS
        /fastp -i "$file1" \
               -o "$out1" \
               -I "$file2" \
               -O "$out2" \
               --detect_adapter_for_pe \
               --overrepresentation_analysis \
               --merge --merged_out "$merged" \
               --unpaired1 "$unmerged" \
               --unpaired2 "$unmerged" \
               --html "$html" \
               --json "$json" \
               --thread $WORKERS \
               2>&1 | tee -a "$log"
        
        # Check if fastp completed successfully
        if [[ $? -eq 0 && -f "$html" ]]; then
            end_time=$(timestamp)
            duration=$((SECONDS - start_seconds))
            echo "[$end_time] Completed analysis of ${subfolder}/${base}${FILE_1_SUFFIX} and ${subfolder}/${base}${FILE_2_SUFFIX}" | tee -a "$log"
            echo "Analysis took $duration seconds" | tee -a "$log"
            echo "----------------------------------------" | tee -a "$log"
        else
            echo "ERROR: Failed to process $subfolder. This is liekly due to insufficient memory." | tee -a "$log"
            echo "Please consider allocating more memory to the system and try again." | tee -a "$log"
            echo "Cleaning up generated files..." | tee -a "$log"
            rm -f "$out1" "$out2" "$merged" "$unmerged" "$html" "$json"
            echo "Retaining log file for debugging purposes." | tee -a "$log"
            echo "----------------------------------------" | tee -a "$log"
        fi
    else
        echo "Skipping $subfolder: Missing *${FILE_1_SUFFIX} or *${FILE_2_SUFFIX} files"
    fi
}

# Process all subdirectories in DATA_FOLDER
for subfolder in "$DATA_FOLDER"/*/; do
    process_subfolder "$subfolder"
done