# get a list of the contents in /data (all read archives)
find /data -name "*.fastq.gz" > /scripts/in1.txt

# get * from /data/*_L001_R (all taxon ids)  # THIS NEEDS TO BE ROBUST TO ALTERNATIVE NAMING SCHEMES
cat /scripts/in1.txt | sed 's/\/data\///g' | sed 's/_L001_R[12]_001\.fastq\.gz//g' > /scripts/in2.txt

# deduplicate entries in /scripts/in2.txt (no duplicates)
sort /scripts/in2.txt | uniq > /scripts/in3.txt

# get list of files in /results
find /results -name "*.fastq.gz" > /scripts/out1.txt

# extract "*" from "/results/*_quality*.fastq.gz" or "/results/*_trimmed*.fastq.gz" only if both exist in "/results"
cat /scripts/out1.txt | sed 's/\/results\///g' | sed 's/_quality.*\.fastq\.gz//g' | sed 's/_trimmed.*\.fastq\.gz//g' > /scripts/out2.txt

# remove entries that occur fewer than 4 times in "/scripts/out2.txt"
cat /scripts/out2.txt | sort | uniq -c | awk '$1 >= 4' | awk '{print $2}' > /scripts/out3.txt

# deduplicate entries in /scripts/out3.txt (no duplicates)
sort /scripts/out3.txt | uniq > /scripts/out4.txt

# get lines that are present in /scripts/in3.txt but not in /scripts/out4.txt
comm -23 /scripts/in3.txt /scripts/out4.txt > /scripts/toProcess.txt

# for each entry in /scripts/in3.txt, call bbduk.sh with in1={entry}_L001_R1_001.fastq.gz and in2={entry}_L001_R2_001.fastq.gz
while read -r filename; do
    # get current timestamp
    timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
    echo "$timestamp :: ⏭️  Queuing the next job... ⏭️\n" | tee -a /logs/log.txt
    retry_count=0
    success=false

    while [ $retry_count -lt 3 ] && [ "$success" = false ]; do
        # ?: there might be an argument for performing read de-duplication here, but computationally intensive
        # ?: see: https://jgi.doe.gov/data-and-tools/software-tools/bbtools/bb-tools-user-guide/dedupe-guide/
        # get file size of $filename in MB
        sizeMB1=$(du -h /data/${filename}_L001_R1_001.fastq.gz | cut -f1)
        sizeMB2=$(du -h /data/${filename}_L001_R2_001.fastq.gz | cut -f1)
        # set the locations of the two input data files
        dataFile1=/data/${filename}_L001_R1_001.fastq.gz
        dataFile2=/data/${filename}_L001_R2_001.fastq.gz
        # set results files
        resultsFile1=/results/${filename}_trimmed_L001_R1_001.fastq.gz
        resultsFile2=/results/${filename}_trimmed_L001_R2_001.fastq.gz
        # create results files
        touch $resultsFile1 $resultsFile2
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        echo "$timestamp :: ✂️  trimming: $line (R1=$sizeMB1/R2=$sizeMB2)...\n🤖  BBDuk Output:\n" | tee -a /logs/log.txt
        # begin a timer to track how long it takes to run bbduk.sh on the file
        SECONDS=$(date +%s)
        # Call the bbduk.sh script with the current filename
        { /bbduk/bbmap/bbduk.sh -Xmx6g overwrite=t in1=$dataFile1 out1=$resultsFile1 in2=$dataFile2 out2=$resultsFile2 ref=/adapters/zfmk_adapters.fa ktrim=r k=23 mink=11 hdist=1 tpe 2>&1 || true; } >> /logs/log.txt
        # Check if the script was successful
        if [ $? -eq 0 ]; then
            # trimming completed successfully
            success=true
            # check elapsed time since timer was started
            duration=$(($(date +%s)-$SECONDS))
            # get current timestamp
            timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
            # get file size of results files in MB
            sizeMB1=$(du -h $resultsFile1 | cut -f1)
            sizeMB2=$(du -h $resultsFile2 | cut -f1)
            echo "\n\n$timestamp :: ✂️  trimmed: $filename (R1=$sizeMB1/R2=$sizeMB2) in $duration seconds!\n" | tee -a /logs/log.txt
            # set merged file output
            mergedFile1=/results/${filename}_trimmed_merged_L001.fastq.gz
            mergedFile2=/results/${filename}_trimmed_unmerged_L001.fastq.gz
            # insert size histogram
            ihistFile=/results/${filename}_insert_size_histogram.txt
            # create results files
            touch $mergedFile1 $mergedFile2 $ihistFile
            # get current timestamp
            timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
            echo "\n$timestamp :: ⏭️  Queuing the read merging job... ⏭️\n" | tee -a /logs/log.txt
            # !: this step could probably be optimized with more work; see: https://jgi.doe.gov/data-and-tools/software-tools/bbtools/bb-tools-user-guide/bbmerge-guide/
            # ?: do not merge quality-trimmed reads; recommended against in docs
            { /bbduk/bbmap/bbmerge.sh -Xmx6g in1=$resultsFile1 in2=$resultsFile2 out=$mergedFile1 outu=$mergedFile2 ihist=$ihistFile 2>&1 || true; } >> /logs/log.txt
            # check elapsed time since timer was started
            duration=$(($(date +%s)-$SECONDS))
            # get current timestamp
            timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
            echo "\n\n$timestamp :: 🤝 merged: $filename in $duration seconds!\n" | tee -a /logs/log.txt
            # get current timestamp
            timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
            echo "\n$timestamp :: ⏭️  Queuing the quality trim job... ⏭️\n" | tee -a /logs/log.txt
            retry_count_qt=0
            success_qt=false
            # get file size of $filename in MB
            sizeMB1=$(du -h /data/${filename}_L001_R1_001.fastq.gz | cut -f1)
            sizeMB2=$(du -h /data/${filename}_L001_R2_001.fastq.gz | cut -f1)
            # set results files
            resultsFile1=/results/${filename}_qualityTrimmed_L001_R1_001.fastq.gz
            resultsFile2=/results/${filename}_qualityTrimmed_L001_R2_001.fastq.gz
            # create results files
            touch $resultsFile1 $resultsFile2
            while [ $retry_count_qt -lt 3 ] && [ "$success_qt" = false ]; do
                # get current timestamp
                timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
                echo "$timestamp :: ✂️ ✂️  quality trimming: $filename (R1=$sizeMB1/R2=$sizeMB2)...\n🤖  BBDuk Output:\n" | tee -a /logs/log.txt
                # begin a timer to track how long it takes to run bbduk.sh on the file
                SECONDS=$(date +%s)
                # Call the bbduk.sh script with the current filename
                { /bbduk/bbmap/bbduk.sh -Xmx6g overwrite=t in1=$dataFile1 out1=$resultsFile1 in2=$dataFile2 out2=$resultsFile2 ref=/adapters/zfmk_adapters.fa qtrim=r trimq=10 ktrim=r k=23 mink=11 hdist=1 tpe 2>&1 || true; } >> /logs/log.txt
                # if quality trimming was successful
                if [ $? -eq 0 ]; then
                    # quality trimming completed successfully
                    success_qt=true
                    # check elapsed time since timer was started
                    duration=$(($(date +%s)-$SECONDS))
                    # get current timestamp
                    timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
                    # get file size of results files in MB
                    sizeMB1=$(du -h $resultsFile1 | cut -f1)
                    sizeMB2=$(du -h $resultsFile2 | cut -f1)
                    echo "\n\n$timestamp :: ✂️ ✂️  quality trimmed: $filename (R1=$sizeMB1/R2=$sizeMB2) in $duration seconds!\n" | tee -a /logs/log.txt
                else
                    # quality trimming failed, increment retry count and wait before retrying
                    retry_count_qt=$((retry_count_qt+1))
                    # get current timestamp
                    timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
                    # Search for "Exception" in the last 6 lines of the log file
                    if tail -n 6 /logs/log.txt | grep -Eq "Exception"; then
                        echo "$timestamp :: 💥 A Java Exception was detected. Waiting 5 seconds and retrying ($retry_count_qt/3)...\n" | tee -a /logs/log.txt
                    else
                        echo "$timestamp :: 💥 Some other problem occurred. Waiting 5 seconds and retrying ($retry_count_qt/3)...\n" | tee -a /logs/log.txt
                    fi
                    sleep 5
                fi
            done
            if [ "$success_qt" = false ]; then
                # get current timestamp
                timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
                # trimming failed after 3 retries
                echo "$timestamp :: 💥 Quality trimming failed all 3 attempts: $filename 💥\n" | tee -a /logs/log.txt
            fi
        else
            # trimming failed, increment retry count and wait before retrying
            retry_count=$((retry_count+1))
            # get current timestamp
            timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
            # Search for "Exception" in the last 6 lines of the log file
            if tail -n 6 /logs/log.txt | grep -Eq "Exception"; then
                echo "$timestamp :: 💥 A Java Exception was detected. Waiting 5 seconds and retrying ($retry_count/3)...\n" | tee -a /logs/log.txt
            else
                echo "$timestamp :: 💥 Some other problem occurred. Waiting 5 seconds and retrying ($retry_count/3)...\n" | tee -a /logs/log.txt
            fi
            sleep 5
        fi
    done

    if [ "$success" = false ]; then
        # get current timestamp
        timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
        # trimming failed after 3 retries
        echo "$timestamp :: 💥 Trimming failed all 3 attempts: $filename 💥\n" | tee -a /logs/log.txt
    fi
done < /scripts/toProcess.txt

# get current timestamp
timestamp=$(date '+%Y-%m-%d_%H:%M:%S(%Z)')
# write success message to log file
echo "$timestamp :: 🎉 BBDuk Trimming Completed! 🎉" | tee -a /logs/log.txt

# delete intermediate files
rm /scripts/in*.txt
rm /scripts/out*.txt
rm /scripts/toProcess.txt