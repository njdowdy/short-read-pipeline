#!/bin/bash

# Define variables for customizable parameters
DATA_FOLDER="/mnt/truenas/data/SEQUENCE_DATA/WGS_EREBIDS_PRJNA702831_WAHLBERG"
TRIMMED_FILENAME_LABEL="_fastpTrimmed"
SPADES_LOCATION="/home/njdowdy/spades-4.0.0/bin/spades.py"  # Replace with actual path to spades.py
MEMORY=144
THREADS=18

# Create temp directory in current working directory
TEMP_DIR="$(pwd)/temp"
mkdir -p "$TEMP_DIR"

# Function to get current timestamp
timestamp() {
  date +"%Y-%m-%d %H:%M:%S"
}

# Function to clean up temp directory
cleanup_temp() {
    rm -rf "${TEMP_DIR:?}"/*
}

# Function to process a single subfolder
process_subfolder() {
    full_path="$1"
    
    # Extract subfolder name without the full path
    subfolder=$(basename "$full_path")
    
    # Define output directory
    final_output_dir="${full_path}/assembly"
    temp_output_dir="${TEMP_DIR}/assembly"
    
    # Check if corrected files already exist in the final output directory
    if ls "${final_output_dir}/corrected/"*.cor.fastq.gz >/dev/null 2>&1; then
        echo "Skipping $subfolder: Corrected files already exist in ${final_output_dir}/corrected/"
        return
    fi
    
    # Find merged and unmerged files
    merged_file=$(find "$full_path" -name "*${TRIMMED_FILENAME_LABEL}_merged.fastq.gz" | head -n 1)
    unmerged_file=$(find "$full_path" -name "*${TRIMMED_FILENAME_LABEL}_unmerged.fastq.gz" | head -n 1)
    
    # Check if both files exist
    if [[ -n $merged_file && -n $unmerged_file ]]; then
        # Extract the base name
        base=$(basename "$merged_file" "${TRIMMED_FILENAME_LABEL}_merged.fastq.gz")
        
        # Copy input files to temp directory
        cp "$merged_file" "$TEMP_DIR/"
        cp "$unmerged_file" "$TEMP_DIR/"
        temp_merged_file="${TEMP_DIR}/$(basename "$merged_file")"
        temp_unmerged_file="${TEMP_DIR}/$(basename "$unmerged_file")"
        
        # Log start of analysis
        start_time=$(timestamp)
        echo "[$start_time] Starting error correction for ${subfolder}/${base}"
        
        # Run SPAdes command and time it
        start_seconds=$SECONDS
        $SPADES_LOCATION --only-error-correction \
                         --memory $MEMORY \
                         --threads $THREADS \
                         --merged "$temp_merged_file" \
                         -s "$temp_unmerged_file" \
                         -o "$temp_output_dir"
        
        # Check if SPAdes completed successfully
        if [ $? -eq 0 ]; then
            end_time=$(timestamp)
            duration=$((SECONDS - start_seconds))
            echo "[$end_time] Completed error correction for ${subfolder}/${base}"
            echo "Analysis took $duration seconds"
            echo "Copying results to final destination..."
            
            # Copy results to final destination
            mkdir -p "$final_output_dir"
            cp -r "$temp_output_dir" "$full_path/"
            
            echo "----------------------------------------"
        else
            echo "ERROR: Failed to process $subfolder. This may be due to insufficient memory or other issues."
            echo "Please check the SPAdes log file for more details."
            
            # Copy log to final destination
            mkdir -p "$final_output_dir"
            cp "${temp_output_dir}/spades.log" "$final_output_dir/" 2>/dev/null
            
            echo "----------------------------------------"
        fi
        
        # Clean up temp directory
        cleanup_temp
    else
        echo "Skipping $subfolder: Missing merged or unmerged files"
    fi
}

# Process all subdirectories in DATA_FOLDER
for subfolder in "$DATA_FOLDER"/*/; do
    process_subfolder "$subfolder"
done

# Final cleanup
rm -rf "$TEMP_DIR"