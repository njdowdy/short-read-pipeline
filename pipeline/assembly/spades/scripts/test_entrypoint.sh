# get a list of the contents in /data (all read archives)
find /data/Hypena_baltimoralis -name "*.fastq.gz" > /scripts/in1.txt

# get * from /data/*_qualityTrimmed*.fastq.gz (all taxonIds)
cat /scripts/in1.txt | sed 's/\/data\/Hypena_baltimoralis\///g' | sed 's/_qualityTrimmed.*\.fastq\.gz//g' > /scripts/in2.txt

# deduplicate entries in /scripts/in2.txt (no duplicates)
sort /scripts/in2.txt | uniq > /scripts/in3.txt

# get list of top level folder names in /results/trimmed
find /results/trimmed -maxdepth 1 -mindepth 1 -type d | sed 's/\/results\/trimmed\///g' > /scripts/trimmed1.txt

# get list of top level folder names in /results/merged
find /results/merged -maxdepth 1 -mindepth 1 -type d | sed 's/\/results\/merged\///g' > /scripts/merged1.txt

# sort the text in /scripts/in3.txt and /scripts/merged1.txt and /scripts/trimmed1.txt alphabetically
sort /scripts/in3.txt > /scripts/in4.txt
sort /scripts/trimmed1.txt > /scripts/trimmed2.txt
sort /scripts/merged1.txt > /scripts/merged2.txt

# get lines that are present in /scripts/in4.txt but not in /scripts/trimmed2.txt
comm -23 /scripts/in4.txt /scripts/trimmed2.txt > /scripts/toProcess_trimmed.txt

# get lines that are present in /scripts/in4.txt but not in /scripts/merged2.txt
comm -23 /scripts/in4.txt /scripts/merged2.txt > /scripts/toProcess_merged.txt

echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 👷 SPAdes is starting... Let's get building!\n" | tee -a /logs/log.txt

while read -r taxonId; do
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: ⏭️  Queuing the next job... ⏭️\n" | tee -a /logs/log.txt
    # set the locations of the two input data files (trimmed and merged)
    dataFile1=/data/Hypena_baltimoralis/${taxonId}_qualityTrimmed_1.fastq.gz
    dataFile2=/data/Hypena_baltimoralis/${taxonId}_qualityTrimmed_2.fastq.gz
    # ?: From SPAdes docs: If you have merged some of the reads from your paired-end 
    # ?: (not mate-pair or high-quality mate-pair) library (using tools s.a. BBMerge or STORM), 
    # ?: you should provide the file with resulting reads as a "merged read file" for the
    # ?: corresponding library. Note that non-empty files with the remaining unmerged
    # ?: left/right reads (separate or interlaced) must be provided for the same library
    # ?: (for SPAdes to correctly detect the original read length).
    mergedReads=/data/Hypena_baltimoralis/${taxonId}_trimmed_merged.fastq.gz
    # ?: From SPAdes docs: If adapter and/or quality trimming software has been used prior to assembly
    # ?: files with the orphan reads can be provided as "single read files" for the corresponding 
    # ?: read-pair library.
    unmergedReads=/data/Hypena_baltimoralis/${taxonId}_trimmed_unmerged.fastq.gz
    # set output directory
    outputDir=/results/trimmed/Hypena_baltimoralis/${taxonId}/
    # make output directory
    mkdir -p $outputDir
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏗️  beginning assembly of trimmed paired reads for: $taxonId...\n" | tee -a /logs/log.txt
    # begin a timer to track how long it takes to run
    SECONDS=$(date +%s)
    # run on trimmed reads
    spades.py -1 $dataFile1 -2 $dataFile2 -o $outputDir 2>&1 >> /logs/log.txt
    # check elapsed time since timer was started
    duration=$((($(date +%s)-$SECONDS)/60))
    echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏢 completed assembly of trimmed paired reads for: $taxonId in $duration minutes!\n" | tee -a /logs/log.txt
    if grep -q "$taxonId" /scripts/toProcess_merged.txt; then
        # set output directory
        outputDir=/results/merged/Hypena_baltimoralis/${taxonId}/
        # make output directory
        mkdir -p $outputDir
        echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏗️  beginning assembly of merged/unmerged paired reads for: $taxonId...\n" | tee -a /logs/log.txt
        # begin a timer to track how long it takes to run
        SECONDS=$(date +%s)
        # run on merged and unmerged reads
        spades.py --merged $mergedReads -s $unmergedReads -o $outputDir 2>&1 >> /logs/log.txt
        # check elapsed time since timer was started
        duration=$((($(date +%s)-$SECONDS)/60))
        echo "\n$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🏢 completed assembly of merged/unmerged paired reads for: $taxonId in $duration minutes!\n" | tee -a /logs/log.txt
    else
        echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 👷 merged/unmerged paired reads were already complete! Moving to next task... \n" | tee -a /logs/log.txt
    fi
done < /scripts/toProcess_trimmed.txt

# delete intermediate files
rm /scripts/in*.txt /scripts/merged*.txt /scripts/trimmed*.txt /scripts/toProcess*.txt

# Send a message to the user
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 👷 SPAdes finished all assemblies! Check the /results folder." | tee -a /logs/log.txt

# extract the contigs.fasta from each of the trimmed assemblies in /results/trimmed and append the folder name to their contig.fasta file
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📦 Packaging all trimmed contigs in a dedicated folder... " | tee -a /logs/log.txt
find /results/trimmed/Hypena_baltimoralis -name "contigs.fasta" | while read -r file; do
    # get the folder name
    folder=$(echo $file | sed 's/\/results\/trimmed\///g' | sed 's/\/contigs.fasta//g')
    # get subtring of folder from start up to but not including the first [a-z]_ or [a-z]-
    folder=$(echo $folder | sed -r 's/^([A-Z]+[0-9]+)[a-z][_-].*/\1/g')
    # match=$(grep "^$folder=" /targets/labels.txt)
    # if [ -n "$match" ]; then
    #     replacement=$(echo "$match" | cut -d= -f2)
    #     taxon=$(echo "$folder" | sed "s/^$folder$/$replacement/")
    # fi
    # append folder name to the contig.fasta filename and copy to /results/trimmed/contigs
    cp -f $file /results/trimmed/Hypena_baltimoralis/contigs/contigs.fasta
done
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📦 Finished packaging all trimmed contigs!" | tee -a /logs/log.txt


# extract the contigs.fasta from each of the merged assemblies in /results/merged and append the folder name to their contig.fasta file
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📦 Packaging all merged contigs in a dedicated folder... " | tee -a /logs/log.txt
find /results/merged/Hypena_baltimoralis -name "contigs.fasta" | while read -r file; do
    # get the folder name
    folder=$(echo $file | sed 's/\/results\/merged\/Hypena_baltimoralis\///g' | sed 's/\/contigs.fasta//g')
    # get subtring of folder from start up to but not including the first [a-z]_ or [a-z]-
    folder=$(echo $folder | sed -r 's/^([A-Z]+[0-9]+)[a-z][_-].*/\1/g')
    # echo folder1: $folder
    # match=$(grep "^$folder=" /targets/labels.txt)
    # # echo folder2: $folder
    # if [ -n "$match" ]; then
    #     replacement=$(echo "$match" | cut -d= -f2)
    #     taxon=$(echo "$folder" | sed "s/^$folder$/$replacement/")
    # fi
    # echo folder3: $folder
    # append folder name to the contig.fasta filename and copy to /results/merged/contigs
    cp -f $file /results/merged/Hypena_baltimoralis/contigs/contigs.fasta
done
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📦 Finished packaging all merged contigs!" | tee -a /logs/log.txt

# compare the contigs.fasta files in /results/trimmed/contigs and /results/merged/contigs for size differences
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📊 Comparing the number of contigs in the trimmed and merged assemblies... " | tee -a /logs/log.txt
find /results/trimmed/Hypena_baltimoralis/contigs -name "*.fasta" | while read -r file; do
    # get the taxon from the text in front of ".contigs.fasta" in the filename
    taxon=$(echo $file | sed 's/\/results\/trimmed\/Hypena_baltimoralis\/contigs\///g' | sed 's/.contigs.fasta//g')
    # get the number of lines in the trimmed contigs.fasta file
    trimmed=$(wc -l $file | awk '{print $1}')
    # get the number of lines in the merged contigs.fasta file
    merged=$(wc -l /results/merged/Hypena_baltimoralis/contigs/${taxon}.contigs.fasta | awk '{print $1}')
    # compare the two numbers
    echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🔎 $taxon contig counts: $trimmed / $merged (trimmed / merged)" | tee -a /logs/log.txt
done
echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 📊 Finished comparing trimmed and merged assemblies!" | tee -a /logs/log.txt


echo "$(date '+%Y-%m-%d_%H:%M:%S(%Z)') :: 🎉 SPAdes Completed! 🎉" | tee -a /logs/log.txt